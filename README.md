# Youtube-Sixads

This is an youtube video scrapper app for scrapping yoube video performance information.

### Key Tools

| Tool | version |
| ------ | ------ |
| python | 3.9 &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
| django | 3.1.11 |
| MySql | 8 |
| celery | 4.4.6 |
| aiohttp | 3.7.4.post0 |

### Features

- Scrapes all video's information for given youtube channels id
- Automatically updates the video information for given channel id
- REST api to provide scrapped video information also filterable with tags and video performance

### Starting the application

We will need a Google APi Key with Youtube Data API V3 enabled. Please get an api key and replace the current one
in `/.envs/.local/.django` file with YOUTUBE_DATA_API_KEY on line no 17

To run the project on development mode run

```sh
docker-compose build
docker-compose up
```

Or you can also combine them both using single command

```sh
docker-compose up --build
```

To create a superuser run the following command and follow the steps

```sh
docker-compose run --rm django python manage.py createsuperuser
```

*Please bear in mind that the production environment is not completely configured. So running the application in
production mode might fail

Finally you will have an admin panel at
`http://localhost:8000/admin`

And two api endpoint which will list the channel videos

1. `localhost:8000/api/channel/videos?tag=tag`

This endpoint will list all videos and will take an optional parameter `tag`. If the `tag` parameter is provided then it
will list videos related to given tag.

2. `localhost:8000/api/channel/videos/stat`

This endpoint will list first 10 videos sorted based on their performance

## Unit test

To run the unit tests run

```sh
docker-compose run --rm django pytest
```

## How the app works

The app uses signal and celery task for initiating youtube video information scrapping. Whenever a Youtube channel id is
added to the database, signal associated with the channel model initiates data scrapping for that specific channels, and
the scrapped data information is saved in database. The view count of a specific video is saved in a separate statistics
table.

Also, a celery task runs every 5 minutes that will scrap all the youtube channel id saved in database except the one
which is added less than 5 minutes before the celery task is running.

At the moment the youtube channel id adding can only be done with django admin panel and its expected that user inputs a
valid channel id because not enough time has been spent on channel id validation.

## How the video performance is calculated

Whenever a new video is scrapped the video view count of that moment is considered as the initital view count. And each
time the scrapping is done the view count of that moment is saved with the scrapping time. So for each video the first
hour view is said as the difference of last view count & initial view count, that is scrapped within the first 60
minutes of initial scrapping.

Finally, the performance of a video is = initial view count of a video / channels all videos first hour views median

### Project Generation

This project was generated with django Cookiecutter. Though the cookiecutter project uses PostgreSQL, but it was
configured to use MySQL

