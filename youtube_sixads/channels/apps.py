from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ChannelsConfig(AppConfig):
    name = "youtube_sixads.channels"
    verbose_name = _("channels")

    def ready(self):
        import youtube_sixads.channels.signals
