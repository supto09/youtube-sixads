import os

import requests
import aiohttp
import asyncio
import time

from youtube_sixads.utils.json_helper import get_string_value, get_list_value

base_url = "https://www.googleapis.com/youtube/v3/"
youtube_api_key = os.environ.get("YOUTUBE_DATA_API_KEY")


def scrap_channel(channel_id):
    """
    For a given channel id returns all videos details
    """

    uploads_id = get_uploads_id(channel_id)

    if uploads_id is None:
        return []

    start_time = time.time()
    video_id_list = get_play_list_items(uploads_id)
    print("---video_id_list_time %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    video_details_list = asyncio.run(get_multiple_video_async(video_id_list))
    print("---all_video_details_time %s seconds ---" % (time.time() - start_time))

    return video_details_list


def get_uploads_id(channel_id):
    """
    For a given channel id returns the uploads id
    """

    api_url = base_url + "channels"
    params = {"id": channel_id, "part": "contentDetails", "key": youtube_api_key}

    response = requests.get(url=api_url, params=params)

    try:
        uploads_id = response.json()["items"][0]["contentDetails"]["relatedPlaylists"][
            "uploads"
        ]
        return uploads_id
    except:
        return None


def get_play_list_items(uploads_id):
    """
    For a given uploads id returns the videos id array
    """

    api_url = base_url + "playlistItems"
    videos = []

    next_page_token = ""
    params = {
        "playlistId": uploads_id,
        "part": "snippet",
        "maxResults": 50,
        "pageToken": next_page_token,
        "key": youtube_api_key,
    }

    while True:
        params["pageToken"] = next_page_token
        response = requests.get(url=api_url, params=params)

        # if response is not 200 then return
        if response.status_code != 200:
            return videos

        response_json = response.json()

        items = response_json["items"]
        for item in items:
            snippet = item["snippet"]
            video_id = snippet["resourceId"]["videoId"]
            videos.append(video_id)

        if "nextPageToken" not in response_json:
            break

        next_page_token = response_json["nextPageToken"]

    return videos


async def get_multiple_video_async(video_id_list):
    """
    For a given video id list returns all videos details
    """

    api_url = base_url + "videos"

    async with aiohttp.ClientSession() as session:
        tasks = []

        for video_id in video_id_list:
            params = {
                "id": video_id,
                "part": "snippet,contentDetails,statistics",
                "key": youtube_api_key,
            }

            tasks.append(
                asyncio.ensure_future(
                    async_get(session=session, url=api_url, params=params)
                )
            )

        response_list = await asyncio.gather(*tasks)

        results = []
        for response in response_list:
            video_details = get_video_details_from_json(response)
            if video_details is not None:
                results.append(video_details)

    return results


async def async_get(session, url, params):
    async with session.get(url, params=params) as response:
        return await response.json()


def get_video_details_from_json(json):
    items = get_list_value(json, "items")

    if len(items) == 0:
        return

    item = items[0]
    snippet = item["snippet"]
    statistics = item["statistics"]

    video_id = get_string_value(item, "id")
    channel_id = get_string_value(snippet, "channelId")
    title = get_string_value(snippet, "title")
    tags = get_list_value(snippet, "tags")
    view_counts = get_string_value(statistics, "viewCount")

    return {
        "video_id": video_id,
        "channel_id": channel_id,
        "title": title,
        "tags": tags,
        "view_counts": view_counts,
    }
