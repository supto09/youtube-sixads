import pytest
from datetime import datetime
from django.test import TestCase, Client

from rest_framework import status
from model_bakery import baker

from youtube_sixads.channels.api.mixins import PerformanceMixin
from youtube_sixads.channels.models import Channel, Video, Statistics

pytestmark = pytest.mark.django_db


class VideoPerformanceTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        channel = baker.make(Channel, channel_id="channel_one")

        video_one = baker.make(
            Video,
            id="video_one",
            channel=channel,
            created_at=datetime.strptime("2021-06-01 10:48:41", "%Y-%m-%d %H:%M:%S"),
        )
        video_two = baker.make(
            Video,
            id="video_two",
            channel=channel,
            created_at=datetime.strptime("2021-06-01 10:48:41", "%Y-%m-%d %H:%M:%S"),
        )

        baker.make(
            Statistics,
            video=video_one,
            view_count=4851,
            created_at=datetime.strptime("2021-06-01 10:48:41", "%Y-%m-%d %H:%M:%S"),
        )
        baker.make(
            Statistics,
            video=video_one,
            view_count=4853,
            created_at=datetime.strptime("2021-06-01 11:18:41", "%Y-%m-%d %H:%M:%S"),
        )
        baker.make(
            Statistics,
            video=video_one,
            view_count=4855,
            created_at=datetime.strptime("2021-06-01 11:40:41", "%Y-%m-%d %H:%M:%S"),
        )
        baker.make(
            Statistics,
            video=video_one,
            view_count=4856,
            created_at=datetime.strptime("2021-06-01 10:58:41", "%Y-%m-%d %H:%M:%S"),
        )

        baker.make(
            Statistics,
            video=video_two,
            view_count=100,
            created_at=datetime.strptime("2021-06-01 10:48:41", "%Y-%m-%d %H:%M:%S"),
        )
        baker.make(
            Statistics,
            video=video_two,
            view_count=110,
            created_at=datetime.strptime("2021-06-01 11:18:41", "%Y-%m-%d %H:%M:%S"),
        )
        baker.make(
            Statistics,
            video=video_two,
            view_count=120,
            created_at=datetime.strptime("2021-06-01 11:40:41", "%Y-%m-%d %H:%M:%S"),
        )
        baker.make(
            Statistics,
            video=video_two,
            view_count=130,
            created_at=datetime.strptime("2021-06-01 10:58:41", "%Y-%m-%d %H:%M:%S"),
        )

    def test_fast_hour_view(self):
        first_video = (
            PerformanceMixin()
            .get_video_query_with_first_hour_view(Video.objects)
            .get(id="video_one")
        )
        self.assertEqual(first_video.initial_view, 4851)
        self.assertEqual(first_video.last_view, 4855)
        self.assertEqual(first_video.first_hour_view, 4)

        second_video = (
            PerformanceMixin()
            .get_video_query_with_first_hour_view(Video.objects)
            .get(id="video_two")
        )
        self.assertEqual(second_video.initial_view, 100)
        self.assertEqual(second_video.last_view, 120)
        self.assertEqual(second_video.first_hour_view, 20)

    def test_video_performance(self):
        data = PerformanceMixin().get_performing_videos(10)
        self.assertEqual(data, ["video_two", "video_one"])

    def test_video_stat_api(self):
        response = self.client.get("/api/channel/videos/stat")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        first_video_from_response = response.data[0]
        second_video_from_response = response.data[1]

        self.assertEqual(first_video_from_response["id"], "video_two")
        self.assertEqual(second_video_from_response["id"], "video_one")
