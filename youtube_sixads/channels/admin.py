from django.contrib import admin

from youtube_sixads.channels.models import Channel, Video, Statistics, Tag

admin.site.register(Channel)
admin.site.register(Video)
admin.site.register(Statistics)
admin.site.register(Tag)
