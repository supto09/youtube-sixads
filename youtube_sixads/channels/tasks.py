from datetime import datetime, timedelta
from config import celery_app

from youtube_sixads.channels.models import Channel, Video, Statistics
from youtube_sixads.channels.scrapper import scrap_channel


@celery_app.task()
def scrap_all_channel_videos():
    """A task to load all channels id from db and start scrapping for each"""

    channels = Channel.objects.all()
    current_time = datetime.now()

    for channel in channels:

        if channel.created_at.replace(tzinfo=None) + timedelta(
            minutes=5
        ) > current_time.replace(tzinfo=None):
            print("Not scrapping ", channel.channel_id)
            continue

        print("Scrapping ", channel.channel_id)
        video_details_list = scrap_channel(channel.channel_id)
        for video_details in video_details_list:
            video = Video.objects.get_or_create(
                id=video_details["video_id"],
                channel=channel,
                defaults={
                    "title": video_details["title"],
                    "initial_view_count": video_details["view_counts"],
                    "created_at": current_time,
                },
            )

            Statistics(
                video_id=video_details["video_id"],
                view_count=int(video_details["view_counts"]),
                created_at=current_time,
            ).save()
