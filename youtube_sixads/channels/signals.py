from django.db.models.signals import post_save
from django.dispatch import receiver

from youtube_sixads.channels.models import Channel, Video, Statistics, Tag
from youtube_sixads.channels.scrapper import scrap_channel


@receiver(post_save, sender=Channel)
def create_channel(sender, instance, created, **kwargs):
    if created:
        video_details_list = scrap_channel(instance.channel_id)
        for video_details in video_details_list:
            video = Video.objects.create(
                id=video_details["video_id"],
                channel=instance,
                title=video_details["title"],
                initial_view_count=video_details["view_counts"],
                created_at=instance.created_at,
            )

            Statistics(
                video=video,
                view_count=video_details["view_counts"],
                created_at=instance.created_at,
            ).save()


@receiver(post_save, sender=Channel)
def save_channel(sender, instance, **kwargs):
    pass
