from django.db import models


class Channel(models.Model):
    channel_id = models.CharField(max_length=100, blank=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return "Channel: " + self.channel_id


class Video(models.Model):
    id = models.CharField(primary_key=True, max_length=100, editable=False)
    channel = models.ForeignKey(Channel, blank=False, on_delete=models.CASCADE)
    title = models.TextField(blank=False)
    initial_view_count = models.BigIntegerField(default=0)
    created_at = models.DateTimeField()

    def __str__(self):
        return "Video: " + str(self.id) + ". " + self.title


class Statistics(models.Model):
    video = models.ForeignKey(Video, blank=False, on_delete=models.CASCADE)
    view_count = models.BigIntegerField(default=0)
    created_at = models.DateTimeField()

    def __str__(self):
        return "Statistics: " + str(self.id) + " for video: " + str(self.video.id)


class Tag(models.Model):
    name = models.CharField(max_length=100, blank=False, editable=False)
    videos = models.ManyToManyField(Video, related_name="tags")

    def __str__(self):
        return self.name
