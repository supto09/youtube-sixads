import operator
import statistics
from datetime import timedelta

from django.db.models import Subquery, OuterRef, F
from setuptools._vendor.more_itertools import take

from youtube_sixads.channels.models import Statistics, Channel, Video
from youtube_sixads.utils.math_helper import divide


class PerformanceMixin:
    def get_video_query_with_first_hour_view(self, queryset):
        return queryset.annotate(
            initial_view=Subquery(
                Statistics.objects.filter(
                    video_id=OuterRef("id"),
                    created_at__lte=OuterRef("created_at") + timedelta(hours=1),
                )
                .order_by("created_at")
                .values("view_count")[:1]
            ),
            last_view=Subquery(
                Statistics.objects.filter(
                    video_id=OuterRef("id"),
                    created_at__lte=OuterRef("created_at") + timedelta(hours=1),
                )
                .order_by("-created_at")
                .values("view_count")[:1]
            ),
            first_hour_view=F("last_view") - F("initial_view"),
        )

    def get_performing_videos(self, video_count):
        # region calculate each channels "all videos first hour views median" and save it in a dict
        channel_median_dict = {}

        channels = Channel.objects.all()
        for channel in channels:
            video_list = self.get_video_query_with_first_hour_view(
                Video.objects.filter(channel_id=channel.id)
            )

            first_hour_view_list = []
            for video in video_list:
                first_hour_view_list.append(video.first_hour_view)

            median = statistics.median(first_hour_view_list)
            print(channel.id, "Median", median)

            channel_median_dict[channel.id] = median

        # endregion

        # region calculate each videos first hour view and save it in dict
        video_view_dict = {}
        videos = self.get_video_query_with_first_hour_view(Video.objects)

        for video in videos:
            video_view_dict[video.id] = divide(
                video.first_hour_view, channel_median_dict[video.channel_id]
            )
        # endregion

        # sort the dictionary with performance value
        sorted_performance_dict = dict(
            sorted(video_view_dict.items(), key=operator.itemgetter(1), reverse=True)
        )

        # take first few video id from the dict
        # maybe make the count dynamic later
        first_items_key = take(video_count, sorted_performance_dict.keys())

        return first_items_key
