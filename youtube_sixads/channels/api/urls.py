from django.urls import path

from youtube_sixads.channels.api.views import (
    VideoListByTagView,
    VideoListByPerformanceView,
)

app_name = "channels"
urlpatterns = [
    path("videos", view=VideoListByTagView.as_view(), name="videos"),
    path("videos/stat", view=VideoListByPerformanceView.as_view(), name="videos-stat"),
]
