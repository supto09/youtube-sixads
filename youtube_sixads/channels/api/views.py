from django.db.models import Case, When
from rest_framework.generics import ListAPIView

from youtube_sixads.channels.api.mixins import PerformanceMixin
from youtube_sixads.channels.api.serializers import VideoSerializer
from youtube_sixads.channels.models import Video


class VideoListByTagView(ListAPIView):
    serializer_class = VideoSerializer

    def get_queryset(self):
        tag = self.request.GET.get("tag", "")
        return Video.objects.filter(tags__name__icontains=tag).distinct()


class VideoListByPerformanceView(ListAPIView, PerformanceMixin):
    serializer_class = VideoSerializer

    def get_queryset(self):
        # get the first few performing video
        performing_videos_id_array = self.get_performing_videos(10)
        print(performing_videos_id_array)

        # Force the query set to return data with same order
        preserved = Case(
            *[
                When(id=id, then=pos)
                for pos, id in enumerate(performing_videos_id_array)
            ]
        )
        return Video.objects.filter(id__in=performing_videos_id_array).order_by(
            preserved
        )
