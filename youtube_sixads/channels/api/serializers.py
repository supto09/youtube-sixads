from rest_framework import serializers

from youtube_sixads.channels.models import Video, Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ["name"]


class VideoSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = ["id", "title", "tags"]
