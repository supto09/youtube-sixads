def divide(dividend, divisor):
    try:
        return dividend / divisor
    except ZeroDivisionError:
        return 0
