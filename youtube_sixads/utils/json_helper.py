def get_string_value(dictionary, key):
    if key in dictionary:
        return dictionary.get(key)

    return ""


def get_list_value(dictionary, key):
    if key in dictionary:
        value = dictionary.get(key)
        return value

    return []
