from youtube_sixads.utils.json_helper import get_string_value, get_list_value


def test_get_string_value():
    test_json_with_key = {"key": "value"}

    test_json_without_key = {}

    assert get_string_value(test_json_with_key, "key") == "value"
    assert get_string_value(test_json_without_key, "key") == ""


def test_get_list_value():
    test_json_with_list = {"key": [{"a"}]}

    test_json_without_list = {}

    assert get_string_value(test_json_with_list, "key") == [{"a"}]
    assert get_list_value(test_json_without_list, "key") == []
