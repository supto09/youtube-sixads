from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "youtube_sixads.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import youtube_sixads.users.signals  # noqa F401
        except ImportError:
            pass
